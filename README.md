# OPC-DCP Bridge

This application will act as a bridge between an OPC server and a SCADA application (MFCS/win 3) using the Sartorius DCU protocol. This allows MFCS/win to use hardware with incompatible OPC server versions.



## Software Requirements

LabVIEW 2021 or later

## License

This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact

For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).
